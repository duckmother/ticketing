<?php

use Illuminate\Http\Request;
use App\Http\Controllers\TicketController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'ApiController@logout');
 
    Route::get('user', 'ApiController@getAuthUser');

    Route::get('tickets' , 'TicketController@index');
    Route::get('tickets/{Ticket}' , 'TicketController@show');//inja reply ha
    Route::group(['middleware' => 'App\Http\Middleware\CheckOperator'] , function(){
        Route::post('tickets' , 'TicketController@store');
        Route::put('tickets/{Ticket}' , 'TicketController@update');
        Route::delete('tickets/{Ticket}' , 'TicketController@destroy');
        Route::post('tickets/reopen/{Ticket}' , 'TicketController@reopen');
    });

    Route::post('replies' , 'ReplyController@store');
    Route::put('replies/{Reply}' , 'ReplyController@update');
    Route::delete('replies/{Reply}' , 'ReplyController@destroy');
});

