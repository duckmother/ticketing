<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Ticket;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CloseTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Closing tickets after 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('tickets')
        ->where('reopen' , '<=' , Carbon::parse('-24 hours'))
        ->update(['closed' => true]); //I used reopen field because it can be updated since reopening functionality
    }
}
