<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\CloseTickets',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('ticket:close')->everyMinute(); //the job of closing deprecated tickets runs every single minute
        //in the production env you should add a cronjob to the server like this:
        // * * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
        //but here in the sake of testing you can simply run:
        // php artisan schedule:run
        // or if you prefere run the single close job:
        // php artisan ticket:close
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
