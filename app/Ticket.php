<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Reply;

class Ticket extends Model
{
    protected $fillable = [
        'subject' , 'body'
    ];

    public function replies() {
        return $this->hasMany(Reply::class);
    }
}
