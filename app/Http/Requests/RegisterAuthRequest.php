<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\JsonResponse;


class RegisterAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|string',
            'email'      => 'required|email|unique:users',
            'password'   => 'required|string|min:6|max:10',
            'isOperator' => 'required'
        ];
    }


    //I override this method to have a custome behaviour on validation failure
    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator) {
        $response = new JsonResponse(['data' => [], 
            'meta'  => [
                'message' => 'The given data is invalid', 
                'errors'  => $validator->errors()
            ]], 422);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
