<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class CheckOperator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->isOperator)
        {
            return response()->json([
                'success' => false,
                'message' => 'you shouldn\'t be an operator to manage tickets'
            ]);
        }
        return $next($request);
    }
}
