<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Http\Requests\ReplyRequest;
use App\Reply;
use App\Ticket;

class ReplyController extends Controller
{

    private $user;

    public function __construct()
    {
        //we store authenticated user once in the sake of readability and prevent repeats
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function store(ReplyRequest $request) {
        $ticket = Ticket::find($request->ticket_id);
        if(!$ticket || 
        ($this->user->id != $ticket->user_id && !$this->user->isOperator) || 
        $ticket->closed) //to store a new reply you should be an operator or the exact ticket sender
            return response()->json([
                'success' => false,
                'message' => 'you can not add replies to this ticket'
            ]);
        $reply = new Reply();
        $reply->body = $request->body;
        $reply->ticket_id = $request->ticket_id;

        if($this->user->replies()->save($reply)) {
            $ticket->replyCount++;
            $ticket->update();
            return response()->json([
                'success' =>true,
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'reply could not be added'
        ]);
    }


    public function update(ReplyRequest $request , Reply $Reply) {
        if(!$Reply || $this->user->id != $Reply->user_id) {
            return response()->json([
                'success' => false,
                'message' => 'Reply not found!'
            ], 400);
        }
        $this->validate($request , [
            'body' => 'required|min:3|max:2000'
        ]); //you don't need to update a reply's parent ticket
        $Reply->body = $request->body;
        if($Reply->update())
            return response()->json([
                'success' => true
            ]);
        return response()->json([
            'success' => false,
            'message' => 'Reply Was not Updated!'
        ]);
    }


    public function destroy(Reply $Reply) {
        if(!$Reply || $this->user->id != $Reply->user_id) { //only the sender of reply can delete it
            return response()->json([
                'success' => false,
                'message' => 'Ticket not found!'
            ], 400);
        }

        if($Reply->delete()) {
            $ticket = Ticket::find($Reply->ticket_id);
            $ticket->replyCount--;
            $ticket->update();
            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Ticket was not deleted'
        ]);
    }
}
