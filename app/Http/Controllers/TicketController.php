<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use JWTAuth;
use App\Http\Requests\TicketRequest;
use Illuminate\Support\Carbon;
use App\Reply;

class TicketController extends Controller
{
    private $user;

    public function __construct()
    {
        //we store authenticated user once in the sake of readability and prevent repeats
        $this->user = JWTAuth::parseToken()->authenticate();
    }


    public function index() {
        return $tickets = $this->user->tickets()->get([
            'subject',
            'body',
            'replyCount', //I added a count cache so fortunately we don't need to query replies here
            'created_at'
        ]);
    }


    public function show(Ticket $Ticket) {
        if(!$Ticket || $this->user->id != $Ticket->user_id) {
            return response()->json([
                'success' => false,
                'message' => 'Ticket not found!'
            ], 400);
        }
        $replies = $Ticket->replies()->get();
        return [$Ticket , $replies];
    }


    public function store(TicketRequest $request) {
        $ticket = new Ticket([
            'subject' => $request->subject,
            'body' => $request->body,
        ]);

        if($this->user->tickets()->save($ticket)) 
            return response()->json([
                'success' => true,
                'message' => $ticket
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry Ticket cannot be added'
            ], 500);
    }

    public function update(TicketRequest $request , Ticket $Ticket) {
        if(!$Ticket || $this->user->id != $Ticket->user_id) { //You can only edit your own tickets
            return response()->json([
                'success' => false,
                'message' => 'Ticket not found!'
            ], 400);
        }
        if($Ticket->fill($request->all())->save())
            return response()->json([
                'success' => true
            ]);
        return response()->json([
            'success' => false,
            'message' => 'Ticket Was not Updated!'
        ]);
    }

    public function destroy(Ticket $Ticket) {
        if(!$Ticket || $this->user->id != $Ticket->user_id) {//You can only delete your own tickets
            return response()->json([
                'success' => false,
                'message' => 'Ticket not found!'
            ], 400);
        }

        if($Ticket->delete()) 
            return response()->json([
                'success' => true
            ]);
        return response()->json([
            'success' => false,
            'message' => 'Ticket was not deleted'
        ]);
    }

    public function reopen(Ticket $Ticket) {
        if(!$Ticket || $this->user->id != $Ticket->user_id) {//You can only reopen your own tickets
            return response()->json([
                'success' => false,
                'message' => 'Ticket not found!'
            ], 400);
        }
        $Ticket->reopen = Carbon::now();//you have 24 fresh hours time to give replies on this certain ticket from now
        $Ticket->closed = false;
        if($Ticket->update())
            return response()->json([
                'success' => true,
                'message' => 'Ticket successfully reopened'
            ]);
        return response()->json([
            'success' => false,
            'message' => 'Ticket could not be reopened'
        ]);
    }
}
